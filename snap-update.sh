#!/usr/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

set -eu  # exit on errors; treat unassigned variables as errors.

tmpfile=$(mktemp)

# This is just a flag to indicate my first time through the loop
first_run=True

while true ; do
    echo "----------------------------- $(date)"

    if [ "$first_run" == "True" ] ; then
        # First time through the loop, just wait for the network come up
        first_run=False
        echo -n "Waiting for network..."
        loop_count=0
        while [ $loop_count -lt 600 ] ; do
            test $(ip addr | grep 'inet ' | grep -v 'inet 127.0.0.1' | wc -l) -gt 0 && break
            echo -n "."
            loop_count=$(( loop_count + 1 ))
            sleep 1
        done
        echo ""
        if [ $loop_count -lt 600 ] ; then
            echo "Network is up."
        else
            echo "Netowrk timed out."
        fi
        sleep 5
    else
        # example output "today at 22:30 EDT" or "tomorrow at 2:37 EDT"
        nexttime=$(sudo /usr/bin/snap refresh --time | grep '^next:' | sed 's/next: //' )

        if ( echo $nexttime | grep -q '^today' ) ; then
            echo -n "waiting until today: "
            nexttime=$( echo $nexttime | sed 's/today at //' | sed 's/ .*//' )
            nexttimestamp=$( date --date $nexttime +%s )
            nowstamp=$( date +%s )
            timedelta=$(( $nexttimestamp - $nowstamp - 75 ))  # I do -75 so I will check a little before the system checks.
        elif ( echo $nexttime | grep -q '^tomorrow' ) ; then
            echo -n "waiting until tomorrow: "
            nexttime=$( echo $nexttime | sed 's/tomorrow at //' | sed 's/ .*//' )
            nexttimestamp=$( date --date $nexttime +%s )
            nowstamp=$( date +%s )
            timedelta=$(( $nexttimestamp - $nowstamp + 86400 - 75 ))
        elif ( echo $nexttime | grep -q 'n/a' ) ; then
            # not sure why this happens. Sometimes it might be "n/a"
            echo -n "waiting until default time: "
            # 900 sec is 15 minutes
            timedelta=900
        else
            # I didn't expect this to happen; I must have some parsing bug above. What should I do?
            # I'll notify the user there's a parsing bug in my script, and wait 1 hour
            notify-send "parsing bug in snap-update.sh"
            echo -n "waiting until unparseable: "
            timedelta=3600
        fi

        # Occasionally, timedelta might be negative, or very small. Ensure a minimum of 900 sec is 15 minutes.
        test $timedelta -lt 900 && timedelta=900

        echo "$timedelta seconds"
        sleep $timedelta
    fi
    
    ( sudo /usr/bin/snap refresh --list &> $tmpfile ) || { notify-send "Failure of snap system. Delay 1hr." ; sleep 1h ; continue ; }
    if grep -q 'All snaps up to date.' $tmpfile ; then
        rm $tmpfile
        echo "All snaps up to date."

        # arbitrary time, just to ensure the system has ample time to do whatever it needs before I check again. e.g. update the next scheduled run time
        sleep 1800
        continue
    fi

    # We keep track of which packages were running when we tried to update them, because
    # we don't need to notify-send about packages that weren't running and updated perfectly.
    # We really only need to notify-send about packages that were delayed because they were running.
    running_packages=""

    finished_packages=""
    packages=$( cat $tmpfile | grep -v '^Name' | sed 's/ .*//' )
    rm $tmpfile

    packages_count=0
    for package in $packages ; do
        packages_count=$(( $packages_count + 1 ))
    done

    packagestring="Need to update $packages_count packages:"
    for package in $packages ; do
        packagestring="$packagestring $package"
    done
    echo $packagestring

    # Even when debugging, echo'ing a "." every second is excessive. Rate limit the echoes.
    loop_counter=0

    echo -n "Looping for packages to be closed..."
    finished_packages_count=0
    while [ $finished_packages_count -lt $packages_count ] ; do
        for package in $packages ; do
            if [ "$package" == "snap-store" ] ; then
                # snap-store launches itself automatically, and blocks everything else from updating.
                killall snap-store || true  # if kill results in error, I don't want script to die.
                sleep 5
            fi
            # If it's currently running, skip it
            if (ps -eo args | grep -q "^/snap/${package}/") ; then
                if ! ( echo "$running_packages" | grep -q " $package " ) ; then
                    running_packages="$running_packages $package "
                fi
                continue
            fi

            # If we already finished this one, skip it
            echo "$finished_packages" | grep -q " $package " && continue

            # We don't need to notify user about updating packages which aren't running.
            if ( echo "$running_packages" | grep -q " $package " ) ; then
                notify-send "Updating ${package}, please wait."
            fi

            echo ""
            echo "Updating ${package}, please wait."
            if ( sudo /usr/bin/snap refresh $package &> $tmpfile ) ; then
                # successfully updated snap
                rm $tmpfile

                # We don't need to notify user about updating packages which aren't running.
                if ( echo "$running_packages" | grep -q " $package " ) ; then
                    notify-send "Successfully updated $package"
                fi

                echo ""
                echo "Successfully updated $package"
                echo -n "Looping for packages to be closed..."
                finished_packages="$finished_packages $package "
                finished_packages_count=$(( $finished_packages_count + 1 ))
            else
                notify-send "Failed updating $package"
                echo ""
                echo "Failed updating $package"
                cat $tmpfile
                echo -n "Looping for packages to be closed..."
                rm $tmpfile
                sleep 60  # whatever went wrong might happen again, don't cycle too fast.fi
            fi
        done
        if [ $loop_counter -eq 300 ] ; then
            echo -n "."
            loop_counter=0
        else
            loop_counter=$(( $loop_counter + 1 ))
        fi
        sleep 1
    done
    echo ""
    echo "Finished: Succesfully updated $finished_packages_count packages: $finished_packages"
    echo "Waiting 15 mins before next check."
    sleep 900
done
