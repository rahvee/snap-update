# snap-update

Here's the problem we're solving with `snap-update.sh`:

In some linux desktops (for example, Ubuntu 22.04 Jammy Jellyfish) you may get a desktop notification like "Pending update of [package_name]. Close the app to avoid disruptions (__ days left)." This notification message is a feature of the snap system, but unfortunately, that feature was not finished at the time it was released to the public. Someday, surely they will finish it, and there will no longer be any need for `snap-update.sh`. The problem is, they notify you to close the app, and they don't trigger an update when you do. So you close the app, and nothing happens. You need to either manually run `sudo snap refresh`, or leave the app closed for several hours until the scheduled update runs again, and there is no notification when that happens.

Here's how `snap-update.sh` helps:

`snap-update.sh` is a script that runs in the background, launched automatically after you login. It follows the schedule of the built-in snap system to check for updates. When updates are available, it loops and keeps checking to see when the app is closed, so it will automatically update when the app is closed. You still get the built-in notification "Pending update of [package_name]. Close the app to avoid disruptions (__ days left)." But when you close the application, you will get additional notifications "Updating [package_name], please wait" and "Successfully updated [package_name]."

### Installation

These instructions were written and tested on Ubuntu 22.04 Jammy Jellyfish. If you have a different system, please fork and submit a merge request, or at least talk to me about it so I can update it. You can reach me at my disposable mozmail address, which is dedicated to this project. Also, I recommend and endorse firefox relay. You should get it for yourself too.   ;-)    (unpaid promotion; I just really like their service)

    2x134w0q4
    @
    mozmail.com



#### Step 1, Download

Follow whichever method you think is better for you:

* Via one-time curl: Optionally, in your home directory, create a `bin` subdir. Then download the script, and make it executable.

            cd ~
            mkdir bin
            cd bin
            curl -O https://gitlab.com/rahvee/snap-update/-/raw/main/snap-update.sh
            chmod +x snap-update.sh

* Via git: Optionally, in your home directory, create a `bin` subdir. Then clone it and link it.

            cd ~
            mkdir bin
            cd bin
            git clone https://gitlab.com/rahvee/snap-update.git
            ln -s snap-update/snap-update.sh .

#### Step 2, sudo

You need to give yourself passwordless sudo rights, to run `snap`.

For example, run `sudo visudo` and create a line such as:

    %sudo   ALL=(ALL:ALL) NOPASSWD:/usr/bin/snap

#### Step 3, Run it

If you want, you can run the script manually on the terminal.

To make it run in the background automatically at login, launch 'Startup Applications Preferences.' Click Add. Give it a name, such as "snap-update" and enter the command `/home/username/bin/snap-update.sh`. If you would like it to write a log file, please edit the `snap-update-wrapper.sh` file to suit your preferences, and run that instead.

When you logout and login again, it will run in the background automatically. If you used the `snap-update-wrapper.sh` option, you can watch the logfile with `tail -f ~/snap-update.log`
